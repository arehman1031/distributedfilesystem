import thread, json, socket, sqlite3, os
from time import sleep
from urllib2 import urlopen

buff_size = 1024
file_buff_size = 1024 * 10
not_found = 404
status_done = 1
file_remain = 1
file_ended = 0
file_already_exist = 401
max_clients = 256

wrc = "wrong command..."
space = ' '
dash = '-'

clients = []

srvr_ops = {'mng_peer': 300, 'c_file': 350, 'd_file': 351, 'mng_file': 352, 'rpl_file': 353}

def entertain_client(client):
	global clients
	username = None
	client_dir = None
	resp = {'status': None}

	clients.append(client)

	while True:
		try:
			req = json.loads(client.recv(buff_size))
		except Exception as e:
			client.close()
			break
		if req['opt'] in srvr_ops.values():
			print 'recv from...',req['srvr_name']
			print req
			peer_id = manage_peer(req['srvr_name'], req['srvr_ip'], req['srvr_port'], False)

			if req['opt'] == srvr_ops['mng_peer']:
				if req['peer_name'] != srvr_name:
					manage_peer(req['peer_name'], req['ip'], req['port'], False)

			elif req['opt'] in [srvr_ops['c_file'], srvr_ops['d_file'], srvr_ops['mng_file'], srvr_ops['rpl_file']]:
				client_id = manage_client(req['client'])
				if req['opt'] == srvr_ops['c_file']:
					sql = "insert into files(name, client_id, version, size, peer_id) values(?,?,?,?,?)"
					execute_sql(sql, (req['filename'], client_id, 0, 0, peer_id))
				elif req['opt'] == srvr_ops['d_file']:
					sql = "delete from files where name=? and client_id=?"
					execute_sql(sql, (req['filename'], client_id))
				elif req['opt'] == srvr_ops['rpl_file']:
					rpl_srvr_id = manage_peer(req['rpl_srvr_name'], req['rpl_srvr_ip'], req['rpl_srvr_port'], False)
					manage_rpl_file(req['filename'], client_id, req['version'], req['size'], rpl_srvr_id)
				elif req['opt'] == srvr_ops['mng_file']:
					manage_file(req['filename'], client_id, peer_id, req['version'], req['size'])

			update_peers(req, peer_id)
#endentertain_client

def manage_rpl_file(filename, client_id, version, size, rpl_srvr_id):
	sql = "select id from files where name=? and client_id=? and peer_id=?"
	file_id = conn.execute(sql, (filename, client_id, rpl_srvr_id)).fetchone()
	if file_id is not None:
		sql = "update files set version=?, size=? where id=?"
		execute_sql(sql, (version, size, file_id[0]))
	else:
		sql = "insert into files(name, client_id, version, size, peer_id) values(?,?,?,?,?)"
		execute_sql(sql, (filename, client_id, version, size, rpl_srvr_id))
	pass

def manage_file(filename, client_id, peer_id, version, size):
	sql = "select id from files where name=? and client_id=?"
	file_id = conn.execute(sql, (filename, client_id)).fetchone()
	if file_id is not None:
		sql = "update files set version=?, size=? where name=? and client_id=?"
		execute_sql(sql, (version, size, filename, client_id))
	else:
		sql = "insert into files(name, client_id, size, version, peer_id) values(?,?,?,?,?)"
		execute_sql(sql, (filename, client_id, size, version, peer_id))

def update_peer(ip, port, msg):
	try:
		peer_socket = socket.socket()
		peer_socket.connect((ip, port))
		peer_socket.send(json.dumps(msg))
		peer_socket.close()
	except Exception as e:
		pass

def execute_sql(sql, param = ()):
	print sql, param
	con = conn.execute(sql, param)
	conn.commit()
	return con.lastrowid
#end execute_sql

def update_peers(msg, peer = 0):
	sql = "select ip, port from peers where id not in (?, ?)"
	try:
		peers = conn.execute(sql, (srvr_id, peer))
		for peer in peers:
			thread.start_new_thread(update_peer, (peer[0], peer[1], msg))
	except Exception as e:
		return

def manage_client(username, updt_peers = True):
	sql = "select id from clients where username=?"
	client_id = conn.execute(sql,(username,)).fetchone()

	if client_id is None:
		sql = "insert into clients(username) values(?)"
		client_id = conn.execute(sql,(username,))
		client_id = client_id.lastrowid
	else:
		client_id = client_id[0]

	# if updt_peers:
	# 	msg = {'opt': srvr_ops['mng_peer'], 'peer_name': peer_name, 'ip': ip, 'port': port, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port, 'srvr_name': srvr_name}
	# 	update_peers(msg)

	return client_id

def manage_peer(peer_name, ip=None, port=None, updt_peers = True):
	if ip is None:
		ip = raw_input('ip: ').strip()
	if port is None:
		port = raw_input('port: ').strip()

	sql = "select id from peers where name=?"
	peer_id = conn.execute(sql, (peer_name,)).fetchone()

	if peer_id is None:
		sql = "insert into peers(name, ip, port) values(?, ?, ?)"
		peer_id = execute_sql(sql, (peer_name, ip, port))
	else:
		peer_id = peer_id[0]
		sql = "update peers set ip=?, port=? where id=?"
		execute_sql(sql, (ip, port, peer_id,))

	if updt_peers:
		msg = {'opt': srvr_ops['mng_peer'], 'peer_name': peer_name, 'ip': ip, 'port': port, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port, 'srvr_name': srvr_name}
		update_peers(msg)
	return peer_id

def start_server():
	global srvr, server_started, srvr_port, srvr_id, srvr_name, srvr_ip
	srvr_port = 0
	try:
		srvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		srvr.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		srvr.bind(('', srvr_port))
		srvr_port = srvr.getsockname()[1]
		print "server port:",srvr_port
		srvr.listen(max_clients)

		srvr_ip = "127.0.0.1"
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		try:
			s.connect(("8.8.8.8", 80))
			srvr_ip = s.getsockname()[0]
		except Exception as e:
			pass
		s.close()

		srvr_name = 'master-db'
		srvr_name = srvr_name.replace(space, dash)

		srvr_id = manage_peer(srvr_name, srvr_ip, srvr_port, False)
		msg = {'opt': srvr_ops['mng_peer'], 'peer_name': srvr_name, 'ip': srvr_ip, 'port': srvr_port, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port, 'srvr_name': srvr_name}
		update_peers(msg)

		server_started = True
		while True:
		   # Establish connection with client.
		   c, addr = srvr.accept()
		   thread.start_new_thread(entertain_client, (c,))
	   #end while
	except Exception as e:
		pass
		server_started = True
#end start_server

def help():
	print
	print "Distributed File System (DFS) - Master DB"
	print
	commands = ["help", "exit"]
	cc = 1
	for command in commands:
		print str(cc) + ":\t" + command
		cc += 1
	#endfor
	print
#endhelp

def initiate():
	global conn, server_started
	server_started = False

	db_name = 'master_dfs.db'

	conn = sqlite3.connect(db_name, check_same_thread=False)
	execute_sql('''CREATE TABLE if not exists peers
		    (id INTEGER PRIMARY KEY AUTOINCREMENT,
			name			CHAR(255),
	        ip				CHAR(15),
        	port			INTEGER)''')
	execute_sql('''CREATE TABLE if not exists clients
		    (id INTEGER PRIMARY KEY AUTOINCREMENT,
			username			CHAR(255))''')

 	execute_sql('''CREATE TABLE if not exists files
			(id INTEGER PRIMARY KEY  AUTOINCREMENT,
 	        name		CHAR(255),
 	        size            REAL,
 	        version			INTEGER,
 	        client_id			INTEGER,
 	        peer_id			INTEGER)''')

	#thread.start_new_thread(manage_files, ())
	return "done"
#end initiate

def initiate_server():
	global server_started
	server_started = False
	thread.start_new_thread(start_server, ())
	while not server_started:
		pass
	#endwhile
#end initiate_server

def start_app():
	print "initiating... "+initiate()

	help()

	initiate_server()

	while True:
		cmd = raw_input('master-db> ').strip()
		cmd = cmd.split(space)
		cmd_len = len(cmd)
		if cmd_len == 2:
			if cmd[0] == 'show':
				sql = "select * from "
				if cmd[1] == 'files':
					sql = "select f.id, f.name, f.size, f.version, c.username, p.name  from files f left join clients c on c.id=f.client_id left join peers p on p.id = f.peer_id"
				elif cmd[1] == "peers":
					sql = sql + "peers"
				elif cmd[1] == "clients":
					sql = sql + "clients"
				else:
					print wrc
					continue
				rows = conn.execute(sql,())
				for row in rows:
					print row
			else:
				print wrc
		elif cmd_len == 1:
			if cmd[0] == 'help':
				help()
			elif cmd[0] == 'exit':
				if server_started is not None:
					srvr.close()
				exit()
			else:
				print wrc
		else:
			print wrc

start_app()
