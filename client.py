import json, socket, subprocess, sqlite3, os, thread
from distutils.spawn import find_executable as find_exec
from time import sleep

buff_size = 1024
big_buff_size = 1024 * 10
file_buff_size = 1024 * 10
not_found = 404
file_remain = 1
file_ended = 0
file_already_exist = 401
file_redirect = 403
status_done = 1

wrc = "wrong command..."
space = ' '
dash = '-'

def execute_sql(sql, param = ()):
	con = conn.execute(sql, param)
	conn.commit()
	return con.lastrowid
#end execute_sql

def send_updated_file(filename):
	global srvr
	filepath = dir+filename

	req = {'opt': 'updt', 'username': username, 'status': file_remain, 'file': filename}
	srvr.send(json.dumps(req))
	with open(filepath, 'r') as f:
		file = f.read(file_buff_size)
		while (file):
			srvr.send(file)
			file = f.read(file_buff_size)
		srvr.shutdown(socket.SHUT_WR)
		# srvr.recv(buff_size)
		srvr.close()
		srvr = get_new_srvr_con()

def read_edit_file(command, filename):
	filepath = dir+filename
	if command == "read":
		subprocess.call(["cat", filepath])
	elif command == "edit":
		execs = ["nano", "notepad.exe"]
		count = 0
		file_exec = execs[count]
		while find_exec(file_exec) is None and count < len(execs):
			count = count+1
			file_exec = execs[count]
		subprocess.call([file_exec, filepath])
		# version += 1
		thread.start_new_thread(send_updated_file, (filename,))
	else:
		print wrc
	#endif
#read_edit_file

def help():
	print
	print "Distributed File System (DFS)"
	print
	commands = ["connect ip:port", "dc", "help",
				"create [filename]", "read [filename]", "edit [filename]", "delete [filename]", "list"]
	cc = 1
	for command in commands:
		print str(cc) + ":\t" + command
		cc += 1
	#endfor
	print
#endhelp

def initiate():
	global dir, conn
	dir="client_dir/"
	db_name = "client_dfs.db"
	conn = sqlite3.connect(db_name, check_same_thread=False)
	execute_sql('''CREATE TABLE if not exists users
		    (id INTEGER PRIMARY KEY AUTOINCREMENT,
			username		CHAR(255))''')

 	execute_sql('''CREATE TABLE if not exists files
			(id INTEGER PRIMARY KEY  AUTOINCREMENT,
 	        name			CHAR(255),
 	        size            REAL,
 	        version			INTEGER,
 	        user_id			INTEGER)''')

	if not os.path.isdir(dir):
		os.mkdir(dir)
	return "done"
#endinitiate

def manage_users(username):
	sql = "select id from users where username=?"
	client_id = conn.execute(sql,(username,)).fetchone()

	if client_id is None:
		sql = "insert into users(username) values(?)"
		client_id = conn.execute(sql,(username,))
		client_id = client_id.lastrowid
	else:
		client_id = client_id[0]

	return client_id

def manage_file(filename, version):
	sql = "select id, version from files where name=? and user_id=?"
	file_id = conn.execute(sql,(filename, user_id)).fetchone()

	if file_id is None:
		sql = "insert into files(filename, version, user_id) values(?, ?, ?)"
		file_id = conn.execute(sql, (filename, version))
		file_id = (file_id.lastrowid, version)
	else:
		return file_id

def get_new_srvr_con():
	global srvr_ip, srvr_port
	tmp_srvr = socket.socket()
	try:
		tmp_srvr.connect((srvr_ip, int(srvr_port)))
	except Exception as e:
		return None
	return tmp_srvr

def handle_file(cmd, filename):
	global srvr
	filepath = dir+filename
	with open(filepath, 'w') as f:
		file = srvr.recv(file_buff_size)
		while (file):
			f.write(file)
			file = srvr.recv(file_buff_size)
		srvr.send('')
		srvr.close()
	srvr = get_new_srvr_con()
	read_edit_file(cmd, filename)

def start_app():
	global username, user_id, srvr, srvr_ip, srvr_port
	srvr = None
	file_not_found_status = "file not found..."

	print "initiating... "+initiate()

	help()

	username = raw_input('username: ').strip().lower()
	username = username.replace(space, dash)
	user_id = manage_users(username)

	while True:
		cmd = raw_input(username+'> ').strip()
		cmd = cmd.split(space)
		cmd_len = len(cmd)
		if cmd_len == 1:
			if cmd[0] in ["dc", "exit"]:
				if srvr is not None:
					req = {'opt': 'dc', 'username': username}
					srvr.send(json.dumps(req))
					srvr.close()
					srvr = None
				if cmd[0] == "exit":
					exit()
					conn.close()

			elif cmd[0] == "help":
				help()
			elif cmd[0] == 'list':
				if srvr is not None:
					req = {'opt': cmd[0], 'username': username}
					srvr.send(json.dumps(req))
					try:
						files = json.loads(srvr.recv(big_buff_size))
						for f in files:
							print f
						else:
							print 'end reached...'
					except Exception as e:
						srvr.close()
						srvr = None
						print 'error occured...'
				else:
					print 'get connected...'
			else:
				print wrc
		elif cmd_len == 2:
			if srvr is None:
				if cmd[0] == 'connect':
					try:
						srvr_ip, srvr_port = cmd[1].split(':')
					except ValueError:
						print "wrong ip:port ->",cmd[1]
						continue
					srvr = socket.socket()
					try:
						srvr.connect((srvr_ip, int(srvr_port)))
					except Exception as e:
						print e
						srvr.close()
						srvr = None
				else:
					print "get connected..."
			else:
				if cmd[0] in ['create', 'delete', 'read', 'edit']:
					filename = cmd[1]
					filepath = dir+filename
					req = {'opt': cmd[0], 'file': filename, 'username': username}
					srvr.send(json.dumps(req))
					if cmd[0] in ['create', 'delete']:
						try:
							resp = json.loads(srvr.recv(buff_size))
						except Exception as e:
							srvr = None
							continue

						if resp['status'] == status_done:
							print cmd[0]+"d..."
						elif resp['status'] == not_found:
							print file_not_found_status
						else:
							print "error occured..."
					elif cmd[0] in  ['read', 'edit']:
						try:
							file = json.loads(srvr.recv(buff_size))
						except Exception as e:
							print "get connected..."
							srvr = None
							continue
						if file['status'] == file_remain:
							handle_file(cmd[0], filename)
							# with open(filepath, 'w') as f:
							# 	file = srvr.recv(file_buff_size)
							# 	while (file):
							# 		f.write(file)
							# 		file = srvr.recv(file_buff_size)
							# 	srvr.send('')
							# 	srvr.close()
							# srvr = get_new_srvr_con()
							# if srvr is not None:
							# 	read_edit_file(cmd[0], filename)
						elif file['status'] == file_redirect:
							print file
							srvr.close()
							srvr_ip = file['peer_ip']
							srvr_port = file['peer_port']
							srvr = get_new_srvr_con()
							req = {'opt': cmd[0], 'file': filename, 'username': username}
							srvr.send(json.dumps(req))
							try:
								file = json.loads(srvr.recv(buff_size))
								if file['status'] not in [not_found, file_redirect]:
									print 'redirecting to...',(srvr_ip, srvr_port)
									handle_file(cmd[0], filename)
								else:
									print 'error occured...'
							except Exception as e:
								print e
								print "get re-connected..."
								srvr.close()
								srvr = None
								continue
							# handle_file(cmd[0], filename)
						elif file['status'] == not_found:
							print file_not_found_status
				else:
					print wrc
		else:
			print wrc


#endstart_app

start_app()
