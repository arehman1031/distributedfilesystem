import thread, json, socket, sqlite3, os
from time import sleep
from urllib2 import urlopen

buff_size = 1024
file_buff_size = 1024 * 10
status_done = 1
file_remain = 1
file_remain_no_version = 2
file_ended = 0
file_already_exist = 401
file_redirect = 403
not_found = 404
max_clients = 256

wrc = "wrong command..."
space = ' '
dash = '-'

clients = []

srvr_ops = {'mng_peer': 300, 'c_file': 350, 'd_file': 351, 'mng_file': 352, 'rpl_file': 353}
client_ops = ['create', 'delete', 'read', 'edit', 'updt', 'dc', 'list']

def entertain_client(client):
	global clients
	username = None
	client_dir = None
	resp = {'status': None}

	clients.append(client)

	while True:
		try:
			req = json.loads(client.recv(buff_size))
			print req
		except Exception as e:
			pass
			client.close()
			break
		if req['opt'] in client_ops:
			username = req['username']
			username = username.replace(space, dash)
			client_id = manage_client(username)
			client_dir = dir+username+"/"
			if not os.path.exists(client_dir):
				os.mkdir(client_dir)
			client_id = manage_client(username)
			if req['opt'] == "dc":
				client.close()
				break
			elif req['opt'] == 'list':
				sql = "select f.name, f.size, f.version, c.username from files f inner join clients c on f.client_id=c.id"
				files = conn.execute(sql, ())
				fs = []
				for f in files:
					fs.append(f)
				client.send(json.dumps(fs))
			elif req['opt'] in ['create', 'delete', 'read', 'edit', 'updt']:
				filename = req['file']
				filepath = client_dir+filename
				sql = "select f.id, f.version, p.ip, p.port from files f left join peers p on f.peer_id=p.id where f.name=? and f.client_id=?"
				file = conn.execute(sql, (filename, client_id)).fetchone()
				if req['opt'] == 'create':
					if file is None:
						with open(filepath, "w") as f:
							sql = "insert into files(name, client_id, version, size, peer_id) values(?,?,?,?,?)"
							execute_sql(sql, (filename, client_id, 0, 0, srvr_id))

							msg = {'opt': srvr_ops['c_file'], 'srvr_name': srvr_name, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port,
							 		'filename': filename, 'client': username, 'version': 0, 'size': 0}
							update_master_db(msg)
						resp['status'] = status_done
						client.send(json.dumps(resp))
					else:
						resp['status'] = file_already_exist
						client.send(json.dumps(resp))
				elif req['opt'] == 'delete':
					if file is not None:
						file_id = file[0]
						sql = "delete from files where name=? and client_id=?"
						execute_sql(sql, (filename, client_id))
						if os.path.exists(filepath):
							os.remove(filepath)

						msg = {'opt': srvr_ops['d_file'], 'srvr_name': srvr_name, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port,
								'filename': filename, 'client': username}
						update_master_db(msg)

						resp['status'] = status_done
						client.send(json.dumps(resp))
					else:
						resp['status'] = not_found
						client.send(json.dumps(resp))
				elif req['opt'] in ['read', 'edit']:
					if os.path.exists(filepath) and file is not None:
						msg = {'status': file_remain}
						client.send(json.dumps(msg))
						with open(filepath, 'r') as f:
							text = f.read(file_buff_size)
							while (text):
								client.send(text)
								text = f.read(file_buff_size)
							client.shutdown(socket.SHUT_WR)
							client.recv(buff_size)
							client.close()
							break
					elif file is not None:
						msg = {'status': file_redirect, 'peer_ip': file[2], 'peer_port': file[3]}
						client.send(json.dumps(msg))
					else:
						resp['status'] = not_found
						client.send(json.dumps(resp))
				elif req['opt'] == 'updt':
					with open(filepath, 'w') as f:
						if file is not None:
							if req['status'] == file_remain_no_version:
								version = file[1]
							else:
								version = file[1] + 1 #previous + 1
							file_id = file[0]
						else:
							version = 0
							file_id = 0
						file = client.recv(file_buff_size)
						while file:
							f.write(file)
							file = client.recv(file_buff_size)
						# client.send('')
						client.close()
					size = os.path.getsize(filepath)
					sql = "update files set version=?, size=? where name=? and client_id=?"
					execute_sql(sql, (version, size, filename, client_id))

					msg = {'opt': srvr_ops['mng_file'], 'srvr_name': srvr_name, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port,
							'filename': filename, 'client': username, 'version': version, 'size': size}
					update_master_db(msg)
					replicate_file(file_id, False)
					break
				else:
					client.send(json.dumps(resp))
			else:
				client.send(json.dumps(resp))
		elif req['opt'] in srvr_ops.values():
			# srvr_dir = dir+req['srvr_name']+"/"
			# if not os.path.exists(srvr_dir):
			# 	os.mkdir(srvr_dir)
			print 'recv from...',req['srvr_name']

			peer_id = manage_peer(req['srvr_name'], req['srvr_ip'], req['srvr_port'], False)

			if req['opt'] == srvr_ops['mng_peer']:
				if req['peer_name'] != srvr_name:
					manage_peer(req['peer_name'], req['ip'], req['port'], False)

			elif req['opt'] in [srvr_ops['c_file'], srvr_ops['d_file'], srvr_ops['mng_file'], srvr_ops['rpl_file']]:
				client_id = manage_client(req['client'])
				if req['opt'] == srvr_ops['c_file']:
					sql = "insert into files(name, client_id, version, size, peer_id) values(?,?,?,?,?)"
					execute_sql(sql, (req['filename'], client_id, 0, 0, peer_id))
				elif req['opt'] == srvr_ops['rpl_file']:
					rpl_srvr_id = manage_peer(req['rpl_srvr_name'], req['rpl_srvr_ip'], req['rpl_srvr_port'], False)
					manage_rpl_file(req['filename'], client_id, req['version'], req['size'], rpl_srvr_id)
				elif req['opt'] == srvr_ops['d_file']:
					sql = "delete from files where name=? and client_id=?"
					execute_sql(sql, (req['filename'], client_id))
				elif req['opt'] == srvr_ops['mng_file']:
					manage_file(req['filename'], client_id, peer_id, req['version'], req['size'])
			client.close()
			break
#endentertain_client

def manage_rpl_file(filename, client_id, version, size, rpl_srvr_id):
	sql = "select id from files where name=? and client_id=? and peer_id=?"
	file_id = conn.execute(sql, (filename, client_id, rpl_srvr_id)).fetchone()
	if file_id is not None:
		sql = "update files set version=?, size=? where id=?"
		execute_sql(sql, (version, size, file_id[0]))
	else:
		sql = "insert into files(name, client_id, version, size, peer_id) values(?,?,?,?,?)"
		execute_sql(sql, (filename, client_id, version, size, rpl_srvr_id))
	pass

def manage_file(filename, client_id, peer_id, version, size):
	sql = "select id from files where name=? and client_id=?"
	file_id = conn.execute(sql, (filename, client_id)).fetchone()
	if file_id is not None:
		sql = "update files set version=?, size=? where name=? and client_id=?"
		execute_sql(sql, (version, size, filename, client_id))
	else:
		sql = "insert into files(name, client_id, size, version, peer_id) values(?,?,?,?,?)"
		execute_sql(sql, (filename, client_id, size, version, peer_id))

def execute_sql(sql, param = ()):
	con = conn.execute(sql, param)
	conn.commit()
	return con.lastrowid
#end execute_sql

def update_master_db(msg):
	if manage_master(False):
		master_db_client.send(json.dumps(msg))
	else:
		print 'could not send to master'



def manage_client(username, updt_peers = True):
	sql = "select id from clients where username=?"
	client_id = conn.execute(sql,(username,)).fetchone()

	if client_id is None:
		sql = "insert into clients(username) values(?)"
		client_id = conn.execute(sql,(username,))
		client_id = client_id.lastrowid
	else:
		client_id = client_id[0]

	# if updt_peers:
	# 	msg = {'opt': srvr_ops['mng_peer'], 'peer_name': peer_name, 'ip': ip, 'port': port, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port, 'srvr_name': srvr_name}
	# 	update_master_db(msg)

	return client_id

def manage_peer(peer_name, ip=None, port=None, updt_master_db = True):
	if ip is None:
		ip = raw_input('ip: ').strip()
	if port is None:
		port = raw_input('port: ').strip()

	sql = "select id from peers where name=?"
	peer_id = conn.execute(sql, (peer_name,)).fetchone()

	if peer_id is None:
		sql = "insert into peers(name, ip, port) values(?, ?, ?)"
		peer_id = execute_sql(sql, (peer_name, ip, port))
	else:
		peer_id = peer_id[0]
		sql = "update peers set ip=?, port=? where id=?"
		execute_sql(sql, (ip, port, peer_id))

	if updt_master_db:
		msg = {'opt': srvr_ops['mng_peer'], 'peer_name': peer_name, 'ip': ip, 'port': port, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port, 'srvr_name': srvr_name}
		update_master_db(msg)
	return peer_id

def start_server():
	global srvr, server_started, srvr_port, srvr_id, srvr_name, srvr_ip
	srvr_port = 0
	try:
		srvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		srvr.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		srvr.bind(('', srvr_port))
		srvr_port = srvr.getsockname()[1]
		print "server port:",srvr_port
		srvr.listen(max_clients)

		srvr_ip = "127.0.0.1"
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		try:
			s.connect(("8.8.8.8", 80))
			srvr_ip = s.getsockname()[0]
		except Exception as e:
			pass
		s.close()

		srvr_name = raw_input('this server name: ').strip().lower()
		srvr_name = srvr_name.replace(space, dash)

		srvr_id = manage_peer(srvr_name, srvr_ip, srvr_port, False)
		manage_master()
		msg = {'opt': srvr_ops['mng_peer'], 'peer_name': srvr_name, 'ip': srvr_ip, 'port': srvr_port, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port, 'srvr_name': srvr_name}
		update_master_db(msg)

		server_started = True
		while True:
		   # Establish connection with client.
		   c, addr = srvr.accept()
		   thread.start_new_thread(entertain_client, (c,))
	   #end while
	except Exception as e:
		pass
		server_started = True
#end start_server

def help():
	print
	print "Distributed File System (DFS)"
	print
	commands = ["add/update master", "add/update peer [peer name]", "show peers/files/clients", "replicate [filename]", "help", "exit"]
	cc = 1
	for command in commands:
		print str(cc) + ":\t" + command
		cc += 1
	#endfor
	print
#endhelp

def initiate():
	global conn, dir, db_name, server_started
	server_started = False

	db_name = 'server_dfs.db'
	dir = "srvr_dir/"

	conn = sqlite3.connect(db_name, check_same_thread=False)
	execute_sql('''CREATE TABLE if not exists peers
		    (id INTEGER PRIMARY KEY AUTOINCREMENT,
			name			CHAR(255),
	        ip				CHAR(15),
        	port			INTEGER)''')
	execute_sql('''CREATE TABLE if not exists clients
		    (id INTEGER PRIMARY KEY AUTOINCREMENT,
			username			CHAR(255))''')

 	execute_sql('''CREATE TABLE if not exists files
			(id INTEGER PRIMARY KEY  AUTOINCREMENT,
 	        name		CHAR(255),
 	        size            REAL,
 	        version			INTEGER,
 	        client_id			INTEGER,
 	        peer_id			INTEGER)''')


	if not os.path.isdir(dir):
		os.mkdir(dir)

	#thread.start_new_thread(manage_files, ())
	return "done"
#end initiate

def manage_master(ask = True):
	global master_db_client, master_db_peer_name
	master_connected = True
	master_db_peer_name = 'master-db'
	master_db_ip = ''
	master_ip_port = 0
	if master_db_client is not None:
		master_db_client.close()
	master_db_client = socket.socket()
	sql = "select ip, port from peers where name=?"
	master_ip_port = conn.execute(sql, (master_db_peer_name,)).fetchone()
	print 'master_ip_port:', master_ip_port
	if master_ip_port is not None:
		master_db_ip= master_ip_port[0]
		master_db_port = master_ip_port[1]
	try:
		master_db_client.connect((master_db_ip, int(master_db_port)))
	except Exception as e:
		if ask:
			while True:
				try:
					print
					print "***Connect Master database***"
					print
					master_db_ip = raw_input('ip: ')
					master_db_port = raw_input('port: ')
					master_db_client.connect((master_db_ip, int(master_db_port)))
					manage_peer(master_db_peer_name, master_db_ip, master_db_port, False)
					break
				except Exception as e:
					continue

		return not master_connected

	return master_connected

def initiate_server():
	global server_started, master_db_client
	master_db_client = None
	server_started = False
	thread.start_new_thread(start_server, ())
	while not server_started:
		pass
	#endwhile

#end initiate_server

def replicate_file(file_id, master_updt = True):
	global dir
	sql = "select p.name, c.username, f.version, f.size, f.client_id, f.name from peers p inner join files f on f.peer_id=p.id inner join clients c on c.id=f.client_id where f.id = ?"
	file = conn.execute(sql, (file_id,)).fetchone()
	print file
	if file is not None:
		existing_peer_name = file[0]
		username = file[1]
		version = file[2]
		size = file[3]
		client_id = file[4]
		filename = file[5]
		filepath = dir+username+'/'+filename
		sql = "select ip, port, name, id from peers where name not in (?,?)"
		ip_port = conn.execute(sql, (master_db_peer_name, srvr_name)).fetchone()
		if ip_port is None:
			print 'no peer found...'
			return

		peer = socket.socket()
		try:
			print 'connecting to',ip_port
			peer.connect((ip_port[0], ip_port[1]))
		except Exception as e:
			print e
			return
		manage_rpl_file(filename,client_id , version, size, ip_port[3])
		msg = {'opt': srvr_ops['rpl_file'], 'srvr_name': srvr_name, 'srvr_ip': srvr_ip, 'srvr_port': srvr_port,
				'rpl_srvr_name': ip_port[2], 'rpl_srvr_ip': ip_port[0], 'rpl_srvr_port': ip_port[1],
				'filename': filename, 'client': username, 'version': version, 'size': size}
		if master_updt:
			update_master_db(msg)

		req = {'opt': 'updt', 'username': username, 'status': file_remain_no_version, 'file': filename}
		peer.send(json.dumps(req))
		with open(filepath, 'r') as f:
			file = f.read(file_buff_size)
			while (file):
				peer.send(file)
				file = f.read(file_buff_size)
			peer.shutdown(socket.SHUT_WR)
			# peer.recv(buff_size)
		peer.close()
		print filepath
	else:
		print 'file not found:',file_id

def start_app():
	print "initiating... "+initiate()

	help()
	initiate_server()

	while True:
		cmd = raw_input(srvr_name+'> ').strip()
		cmd = cmd.split(space)
		cmd_len = len(cmd)

		if cmd_len == 1:
			if cmd[0] == "help":
				help()
			elif cmd[0] == "exit":
				if server_started:
					srvr.close()
				for client in clients:
					client.close()
				conn.close()
				exit()
			else:
				print wrc
		elif cmd_len == 2:
			if cmd[0] == 'show':
				sql = "select * from "
				if cmd[1] == 'files':
					sql = "select f.id, f.name, f.size, f.version, c.username, p.name  from files f left join clients c on c.id=f.client_id left join peers p on p.id = f.peer_id"
				elif cmd[1] == "peers":
					sql = sql + "peers"
				elif cmd[1] == "clients":
					sql = sql + "clients"
				else:
					print wrc
					continue
				rows = conn.execute(sql,())
				for row in rows:
					print row
			elif cmd[0] in ['add', 'update'] and cmd[1] == 'master':
				manage_master()
			elif cmd[0] == 'replicate':
				replicate_file(cmd[1])
			else:
				print wrc
		elif cmd_len == 3:
			if cmd[0] in ['add', 'update'] and cmd[1] == 'peer':
				manage_peer(cmd[2])
			else:
				print wrc
		else:
			print wrc

start_app()
