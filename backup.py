import thread, socket, os, sqlite3, subprocess, json
from distutils.spawn import find_executable as find_exec
from sys import exit
from time import sleep
from urllib2 import urlopen

max_space = 10*1024
buff_size = 1024
client_wait = 2

def entertain_client(client):
	resp = {'found': None}
	req = json.loads(client.recv(buff_size))

	if req['file'] is not None:
		sql = "select id from files where name=?"
		row = conn.execute(sql, (req['file'],)).fetchone()
		if row is not None:
			resp['found'] = 1
	client.send(json.dumps(resp))
	client.close()
#end entertain_client

def execute_sql(sql, param = (), p_peer = None):
	conn.execute(sql, param)
	conn.commit()
	if p_peer is not None:
		pass
	else:
		pass
#end execute_sql

def update_peers(sql):
	pass

def start_server():
	global srvr, server_started, srvr_port
	srvr_port = 0
	try:
		srvr = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		srvr.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		srvr.bind(('', srvr_port))
		srvr_port = srvr.getsockname()[1]
		print "server port: ",srvr_port
		server_started = True
		srvr.listen(2)
		peer_name = raw_input('server name: ')
		sql = "select id, name from peers where name=?"
		id = conn.execute(sql, (peer_name,)).fetchone()
		if id is None:
			sql = "insert into peers(name, ip, port) values(?, ?, ?)"
			ip=urlopen('http://ip.42.pl/raw').read()
			execute_sql(sql, (peer_name, ip, srvr_port))
		else:
			sql = "update peers set ip=?, port=? where id=?"
			ip=urlopen('http://ip.42.pl/raw').read()
			execute_sql(sql, (ip, srvr_port, id[0]))
		while True:
		   # Establish connection with client.
		   c, addr = srvr.accept()
		   thread.start_new_thread(entertain_client, (c,))
	   #end while
	except Exception as e:
		print "server not started..."
		server_started = True
#end start_server

def manage_files():
	while True:
		sleep(5) #sec
		sql = "select sum(size) from files"
		row = conn.execute(sql).fetchone()
		size = 0
		if row[0] is not None:
			size = row[0]
	#endwhile

def help():
	print
	print "Distributed File System (DFS)"
	print
	commands = ["create [filename]", "read [filename]", "edit [filename]", "delete [filename]",
				"start server",
	 			"add peer [peer name]", "update peer [peer name]", "show peers/files", "help", "exit"]
	cc = 1
	for command in commands:
		print str(cc) + ":\t" + command
		cc += 1
	#endfor
	print
#endhelp

def initiate():
	global conn, dir, db_name, server_running
	db_name = 'dfs.db'
	dir = "dir"

	conn = sqlite3.connect(db_name, check_same_thread=False)
	execute_sql('''CREATE TABLE if not exists peers
		    (id INTEGER PRIMARY KEY AUTOINCREMENT,
			name			TEXT,
	        ip				CHAR(15),
        	port			INTEGER)''')

 	execute_sql('''CREATE TABLE if not exists files
			(id INTEGER PRIMARY KEY  AUTOINCREMENT,
 	        name		CHAR(255),
 	        size            REAL,
 	        version			INTEGER,
 	        user_id			INTEGER)''')


	if not os.path.isdir(dir):
		os.mkdir(dir)

	#thread.start_new_thread(manage_files, ())
	dir = dir + "/"
	return "done"
#end initiate

def ask_peer(ip, port, filename):
	global peer_id
	print 'asking...'+str(peer_id)
	this_peer = peer_id
	s = socket.socket()
	try:
		s.connect((ip, port))
	except Exception as e:
		return
	req = { 'file': filename }
	s.send(json.dumps(req))
	resp = json.loads(s.recv(buff_size))
	if resp['found'] is not None:
		print "found"
	else:
		print "not found"
	s.close()

def manage_file(command, filepath, f_id, version):
	if command == "read":
		subprocess.call(["cat", filepath])
	elif command == "edit":
		execs = ["nano", "notepad.exe"]
		count = 0
		file_exec = execs[count]
		while find_exec(file_exec) is None and count < len(execs):
			count = count+1
			file_exec = execs[count]
		subprocess.call([file_exec, filepath])
		size = os.path.getsize(filepath)
		sql = "update files set size=?, version=? where id=?"
		version+=1
		execute_sql(sql, (size, version, f_id))
	elif command == "delete":
		if file_exist:
			subprocess.call(["rm", filepath])
			sql = "delete from files where id=?"
			execute_sql(sql, (f_id,))
		#endif
	else:
		print wrc
	#endif
#endmanage_file

def initiate_server():
	global server_started
	server_started = False
	thread.start_new_thread(start_server, ())
	while not server_started:
		pass
	#endwhile
#end initiate_server

def start_app():
	global peer_id, username
	peer_id = 0
	wrc = "wrong command..."

	print "initiating... " + initiate()




	help()

	resp = raw_input("Do you want to start the server? ")
	if resp in ['y', 'Y', 'yes', 'YES', 'Yes']:
		initiate_server()

	while True:
		command = raw_input('DFS> ')
		command = command.lower()
		command = command.split(' ')
		if len(command) == 2:
			if command[0] == "start" and command[1] == "server":
				if not server_started:
					initiate_server()
				else:
					print "Server already running on port: "+str(srvr_port)
			elif command[0] in ["create", "read", "edit", "delete"]:
				filename = command[1]
				filepath = dir+filename
				file_exist = os.path.exists(filepath)
				if command[0] == "create":
					if not file_exist:
						file = open(filepath, "w")
						sql = "insert into files(name, size, version, user_id) values(?, 0, 0, 0)"
						execute_sql(sql, (filename, ))
						file.close()
					else:
						print "File already exist."
					#end if
				elif file_exist:
					sql = "select id, version from files where name=?"
					row = conn.execute(sql, (filename,)).fetchone()
					if len(row) > 0:
						f_id = row[0]
						version = row[1]
						manage_file(command[0], filepath, f_id, version)
						#endif
					#endif
				else:
					sql = "select ip, port, id from peers"
					rows = conn.execute(sql)
					ip = 0
					port = 0
					for row in rows:
						if row[0] is not None and row[1] is not None:
							if peer_id != -1:
								peer_id = row[2]
								thread.start_new_thread(ask_peer, (row[0], row[1], filename))
								sleep(client_wait)
							else:
								peer_id = 0
								break
							#endif peer_id != -1
						#endif row[0] is not None and row[1] is not None:
					#endfor row in rows:
				#end ifelif
			elif command[0] == "show":
				r_to_e = True
				sql = "select * from "
				if command[1] == "files":
					sql = sql + "files"
					#endfor
				elif command[1] == "peers":
					sql = sql + "peers"
				else:
					r_to_e = False

				if r_to_e:
					rows = conn.execute(sql)
					for row in rows:
						print row
				else:
					print wrc
				#ednif
			else:
				print wrc
			#end if command[0] in ["create", "read", "edit", "delete"]:
		elif len(command) == 3:
			if command[0] in ["add", "update"]:
				if command[1] == "peer":
					peer_name = command[2]
					sql = "select id from peers where name=?"
					id = conn.execute(sql, (peer_name,)).fetchone()
					if command[0] == "add":
						if id is None:
							ip = raw_input('ip: ')
							port = raw_input('port: ')
							sql = "insert into peers(name, ip, port) values(?, ?, ?)"
					 		execute_sql(sql, (peer_name, ip, port))
						else:
							print "peer name already exist..."
						#endif
					elif command[0] == "update":
						if id is not None:
							id = id[0]
							ip = raw_input('ip: ')
							port = raw_input('port: ')
							sql = "update peers set ip=?, port=? where id=?"
					 		execute_sql(sql, (ip, port, id))
						else:
							print "peer name not found..."
					else:
						print wrc
					#endif
				else:
					print wrc
				#endif
			else:
				print wrc
			#endif command[0] in ["add", "update"]:
		elif len(command) == 1:
			if command[0] == "help":
				help()
			elif command[0] == "exit":
				conn.close()
				break;
			else:
				print wrc
			#endif
		else:
			print wrc
		#emdif
	exit()

	#end if
#end main

start_app()
